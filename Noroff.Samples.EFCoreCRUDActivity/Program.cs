﻿using Noroff.Samples.EFCoreCRUDActivity.Data;

namespace Noroff.Samples.EFCoreCRUDActivity
{
    public class Program
    {
        static void Main(string[] args)
        {
            var manager = new ProductManager();

            // Add a new product
            manager.AddProduct("Laptop", 1000);

            // List all products
            manager.ListProducts();

            // Update a product
            manager.UpdateProduct(1, "Gaming Laptop", 1500);

            // Delete a product
            manager.DeleteProduct(1);
        }
    }

}