﻿using Noroff.Samples.EFCoreCRUDActivity.Data;
using Noroff.Samples.EFCoreCRUDActivity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Noroff.Samples.EFCoreCRUDActivity.Data
{
    public class ProductManager
    {
        public void AddProduct(string name, decimal price)
        {
            using (var context = new AppDbContext())
            {
                var product = new Product { Name = name, Price = price };
                context.Products.Add(product);
                context.SaveChanges();
            }
        }

        public void ListProducts()
        {
            using (var context = new AppDbContext())
            {
                var products = context.Products.ToList();
                foreach (var product in products)
                {
                    Console.WriteLine($"ID: {product.Id}, Name: {product.Name}, Price: {product.Price}");
                }
            }
        }

        public void UpdateProduct(int id, string newName, decimal newPrice)
        {
            using (var context = new AppDbContext())
            {
                var product = context.Products.Find(id);
                if (product != null)
                {
                    product.Name = newName;
                    product.Price = newPrice;
                    context.SaveChanges();
                }
            }
        }

        public void DeleteProduct(int id)
        {
            using (var context = new AppDbContext())
            {
                var product = context.Products.Find(id);
                if (product != null)
                {
                    context.Products.Remove(product);
                    context.SaveChanges();
                }
            }
        }

    }
}
